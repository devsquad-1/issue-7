package com.project.databasetrigger.order;

import com.project.databasetrigger.historyLog.Log;
import com.project.databasetrigger.historyLog.LogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private LogRepository logRepository;

    public List<Order> getOrders() {
        return orderRepository.findAll();
    }

    public Order getOrderByID(Long orderID) {
        return orderRepository.getById(orderID);
    }

    public void addOrder(Order order) {

        orderRepository.save(order);
        triggerLog(order, "added");
    }

    public void deleteOrder(Long orderID) {

        Order order = orderRepository.getById(orderID);
        orderRepository.deleteById(orderID);

        triggerLog(order, "deleted");
    }

    @Transactional
    public void editDetails(Long orderID, double totalPrice) {

        Order order = orderRepository.findById(orderID)
                .orElseThrow(() -> new IllegalStateException("ID not found"));

        triggerLog(order, "modified");
        order.setTotalPrice(totalPrice);
    }

    // this method will be invoked after every insertion, modification and deletion
    public void triggerLog(Order order, String action){

        // create new log with the relevant details
        Log log = new Log(
                UUID.randomUUID(),
                "Order with ID " + order.getOrderID() + " has been " + action,
                "details: " + order,
                LocalDateTime.now()
        );

        // insert the new log into the 'log' database
        logRepository.save(log);
    }

    public List<Log> getHistoryLog() {
        return logRepository.findAll();
    }

    public List<Log> getInsertionLog(){
        List<Log> allLog = logRepository.findAll();
        return allLog.stream().filter(e -> e.getDescription().contains("added")).collect(Collectors.toList());
    }

    public List<Log> getModificationLog() {
        List<Log> allLog = logRepository.findAll();
        return allLog.stream().filter(e -> e.getDescription().contains("modified")).collect(Collectors.toList());
    }

    public List<Log> getDeletionLog() {
        List<Log> allLog = logRepository.findAll();
        return allLog.stream().filter(e -> e.getDescription().contains("deleted")).collect(Collectors.toList());
    }
}
