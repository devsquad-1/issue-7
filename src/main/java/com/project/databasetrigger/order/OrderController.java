package com.project.databasetrigger.order;

import com.project.databasetrigger.historyLog.Log;
import com.project.databasetrigger.historyLog.LogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "orders")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @GetMapping
    public List<Order> getAllOrders (){
        return orderService.getOrders();
    }

    @GetMapping(path = "{orderID}")
    public String getOrderByID(@PathVariable("orderID") Long orderID){
        Order order = orderService.getOrderByID(orderID);
        return "Details " + order.toString();
    }

    @GetMapping(path = "history")
    public List<Log> getHistoryLog(){
        return orderService.getHistoryLog();
    }

    @GetMapping(path = "history/insertion")
    public List<Log> getInsertionLog(){
        return orderService.getInsertionLog();
    }

    @GetMapping(path = "history/modification")
    public List<Log> getModificationLog(){
        return orderService.getModificationLog();
    }

    @GetMapping(path = "history/deletion")
    public List<Log> getDeletionLog(){
        return orderService.getDeletionLog();
    }

    @PostMapping
    public void addNewOrder(@RequestBody Order order){
        orderService.addOrder(order);
    }

    @PutMapping(path = "update/{orderID}")
    public void updateOrder(@PathVariable("orderID") Long orderID, @RequestParam("totalPrice") double totalPrice){
        orderService.editDetails(orderID, totalPrice);
    }

    @DeleteMapping(path = "delete/{orderID}")
    public void deleteOrder(@PathVariable Long orderID){
        orderService.deleteOrder(orderID);
    }
}
