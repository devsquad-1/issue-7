package com.project.databasetrigger.order;

import javax.persistence.*;

@Entity
@Table(name = "orders")
public class Order {

    @Id
    private Long orderID;
    private String CustomerName;
    private Integer totalItems;
    private Double totalPrice;

    public Order() {
    }

    public Order(Long orderID, String customerName, Integer totalItems, Double totalPrice) {
        this.orderID = orderID;
        CustomerName = customerName;
        this.totalItems = totalItems;
        this.totalPrice = totalPrice;
    }

    public Long getOrderID() {
        return orderID;
    }

    public void setOrderID(Long orderID) {
        this.orderID = orderID;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    public Integer getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(Integer totalItems) {
        this.totalItems = totalItems;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    @Override
    public String toString() {
        return "Order ID: " + orderID + ", customer name: " + CustomerName + ", total items: "  + totalItems +
                ", total price: " + totalPrice;
    }
}
